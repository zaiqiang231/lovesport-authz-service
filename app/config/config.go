package config

import (
	baseConfig "gitee.com/zaiqiang231/go-base-app/base_app/config"
	"time"
)

var (
	GlobalAppConfig = new(AppConfig)
	ServiceName     = "lovesport-authz-service"
)

type AppConfig struct {
	baseConfig.AppConfig `mapstructure:",squash"` //结构体的字段提到父结构中
	RedisConfig          *RedisConfig             `mapstructure:"redis"`
	AnalyticsConfig      *AnalyticsConfig         `mapstructure:"analytics"`
}

type RedisConfig struct {
	Addr     string `mapstructure:"addr"`
	Password string `mapstructure:"password"`
}

type AnalyticsConfig struct {
	PoolSize              int           `mapstructure:"pool-size"`
	RecordsBufferSize     uint64        `mapstructure:"records-buffer-size"`
	FlushInterval         time.Duration `mapstructure:"flush-interval"`
	StorageExpirationTime time.Duration `mapstructure:"storage-expiration-time"`
	EnableDetailRecording bool          `mapstructure:"enable-detailed-recording"`
}
