package httpHandler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func WriteResponse(c *gin.Context, code int, msg string, data interface{}) {
	c.JSON(http.StatusOK, gin.H{
		"code":    code,
		"message": msg,
		"data":    data,
	})
}
