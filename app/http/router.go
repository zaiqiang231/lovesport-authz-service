package http

import (
	httpHandler "gitee.com/zaiqiang231/lovesport-authz-service/app/http/handler"
	"github.com/gin-gonic/gin"
)

func InitRouter(g *gin.Engine) {
	g.Use(gin.Logger(), gin.Recovery())
	g.POST("/authz", httpHandler.AuthFunc, httpHandler.Authorize)
}
