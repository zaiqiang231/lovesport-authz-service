package constant

import "github.com/pkg/errors"

var (
	ErrFailedGetStore = errors.New("failed to get store")
)
