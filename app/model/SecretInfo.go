package model

type SecretInfo struct {
	SecretID  string
	SecretKey string
}
