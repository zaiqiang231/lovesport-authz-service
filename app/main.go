package main

import (
	"context"
	"gitee.com/zaiqiang231/go-base-app/base_app"
	baseLog "gitee.com/zaiqiang231/go-base-app/base_app/log"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/analytics"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/config"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/http"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/rpc"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/store"
	"github.com/fatih/color"
	"github.com/spf13/viper"
	"golang.org/x/sync/errgroup"
)

func main() {
	base_app.NewApp(
		config.ServiceName,
		initConfig,
		base_app.WithCmdDescription("LoveSport user service"),
		base_app.WithRunFunc(run),
	).Run()
}

// 解析配置
func initConfig(a *base_app.App) {
	baseLog.Debugf("开始读取配置")
	if err := viper.Unmarshal(config.GlobalAppConfig); err != nil {
		baseLog.Fatalf("读取配置失败: %v", err)
	}
	baseLog.Debugf("读取配置: GlobalConfig = %v", config.GlobalAppConfig)
	var err error
	//初始化mysql
	_, err = store.InitDataStore(config.GlobalAppConfig.MysqlConfig)
	if err != nil {
		baseLog.Errorf("failed to init store : %v", err)
	}
	//初始化redis
	_, err = store.InitRedisStore(config.GlobalAppConfig.RedisConfig)
	if err != nil {
		baseLog.Errorf("failed to init redis : %v", err)
	}

	//初始化rpc
	_, err = rpc.InitRpc(config.GlobalAppConfig.GRpcServerConfig.Addr)
	if err != nil {
		baseLog.Errorf("failed to init rpc : %v", err)
	}

	//初始化cache
	_, err = store.InitCache()
	if err != nil {
		baseLog.Errorf("failed to init cache : %v", err)
	}
	store.NewLooper(context.Background(), store.GetCache()).Start()

	//授权请求数据记录
	analyticsIns := analytics.NewAnalytics(config.GlobalAppConfig.AnalyticsConfig)
	analyticsIns.Start()

}

// 配置已经读取完成后，再启动服务
func run(basename string) error {
	baseLog.Infof("%v %v 应用启动", color.GreenString("LoveSport:"), basename)
	defer func() {
		baseLog.Infof("%v %v 应用退出", color.GreenString("LoveSport:"), basename)
	}()

	var eg errgroup.Group
	gs := base_app.NewGracefulShutdown()
	gs.AddShutdownCallback(base_app.ShutdownFunc(func() error {
		return nil
	}))
	//http 服务
	base_app.CreateAPIServer(
		basename,
		base_app.WithServerConfig(config.GlobalAppConfig.ServerConfig),
		base_app.WithGracefulShutdown(gs),
		base_app.WithRouter(http.InitRouter),
	).PrepareRun().Run(&eg)

	//优雅关闭监听启动
	if err := gs.Start(); err != nil {
		baseLog.Fatalf("%v start shutdown manager failed: %s", basename, err.Error())
	}

	if err := eg.Wait(); err != nil {
		baseLog.Fatalf("ApiServer eg.Wait() err: ", err.Error())
	}

	return nil
}
