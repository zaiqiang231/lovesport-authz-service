package test

import (
	"fmt"
	"github.com/dgraph-io/ristretto"
	"testing"
)

func TestRistrettoSetting(t *testing.T) {
	fmt.Printf("value: %v \n", int64(1e7))
	fmt.Printf("value: %v \n", 1<<30)
}

func TestRistrettoCache(t *testing.T) {
	cache, err := ristretto.NewCache(&ristretto.Config{
		NumCounters: 1e7,     // number of keys to track frequency of (10M).
		MaxCost:     1 << 27, // maximum cost of cache (1GB 1<<30).
		BufferItems: 64,      // number of keys per Get buffer.
	})
	if err != nil {
		panic(err)
	}

	// set a value with a cost of 1
	cache.Set("key1", "1234567890", 1)
	cache.Set("key2", "123456", 1)
	cache.Set("key3", "value3", 1)
	cache.Set("key4", "value4", 1)

	// wait for value to pass through buffers
	cache.Wait()
	var (
		value interface{}
		found bool
	)

	value, found = cache.Get("key1")
	fmt.Printf("value: %v, found %v \n", value, found)
	value, found = cache.Get("key2")
	fmt.Printf("value: %v, found %v \n", value, found)
	value, found = cache.Get("key3")
	fmt.Printf("value: %v, found %v \n", value, found)
	value, found = cache.Get("key4")
	fmt.Printf("value: %v, found %v \n", value, found)
}
