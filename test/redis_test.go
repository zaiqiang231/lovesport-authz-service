package test

import (
	"context"
	"fmt"
	"gitee.com/zaiqiang231/lovesport-authz-service/app/analytics"
	"github.com/go-redis/redis/v8"
	"github.com/vmihailenco/msgpack/v5"
	"testing"
)

func TestRedis(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	v := rdb.LIndex(context.Background(), analytics.AnalyticsKeyName, 0).Val()
	decoded := analytics.AnalyticsRecord{}
	err := msgpack.Unmarshal([]byte(v), &decoded)
	fmt.Printf("result : %+v |||| %+v \n", decoded, err)

	pipe := rdb.Pipeline()
	cmdResult := pipe.LRange(context.Background(), analytics.AnalyticsKeyName, 0, -1)
	pipe.Del(context.Background(), analytics.AnalyticsKeyName)

	if _, err := pipe.Exec(context.Background()); err != nil {
		fmt.Printf("Error trying to append to set keys: %s", err.Error())
	}
	values := cmdResult.Val()
	fmt.Printf("values : %+v \n", values)
}
